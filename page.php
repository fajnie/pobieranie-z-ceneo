<?php

    class Page
    {
        protected static $ceneoNameClass = "cat-prod-row-name";
        protected static $ceneoNameClass2 = "category-item-box-name";

        protected static $ceneoPriceClass = "cat-prod-row-price";
        protected static $ceneoPriceClass2 = "category-item-box-price";

        protected static $ceneoImgClass = "category-item-box-picture";
        protected static $ceneoImgClass2 = "cat-prod-row-foto";

        protected static $otherPageClass = "pagination";

        private $finder = null;
        private $pageContent = null;
        private $url = null;

        private $productName = [];
        private $productPrice = [];
        private $productImg = [];

        function __construct($url)
        {
            $this->pageContent = file_get_contents($url);
            $dom = new DOMDocument();
            $dom->loadHTML($this->pageContent);
            $this->finder = new DomXPath($dom);

        }
        public function getProductName()
        {
            $classname = self::$ceneoNameClass;
            $productList = $this->finder->query("//*[contains(concat(' ', normalize-space(@class), ' '), ' $classname')]");
            if($productList->length == 0)
            {
                $classname = self::$ceneoNameClass2;
                $productList = $this->finder->query("//*[contains(concat(' ', normalize-space(@class), ' '), ' $classname')]");
            }
            foreach ($productList as $key => $value)
            {
                $this->productName[] = $value->nodeValue;
            }
            return $this->productName;
        }
        public function getProductPrice()
        {
            $classname = self::$ceneoPriceClass;
            $productList = $this->finder->query("//*[contains(concat(' ', normalize-space(@class), ' '), ' $classname')]");
            if($productList->length == 0)
            {
                $classname = self::$ceneoPriceClass2;
                $productList = $this->finder->query("//*[contains(concat(' ', normalize-space(@class), ' '), ' $classname')]");
            }
            foreach ($productList as $key => $value)
            {
                if($classname == self::$ceneoPriceClass)
                    $this->productPrice[] = $value->getElementsByTagName('span')->item(1)->nodeValue;
                else
                    $this->productPrice[] = $value->getElementsByTagName('span')->item(2)->nodeValue;
            }
            return $this->productPrice;
        }
        public function getProductImg()
        {
            $classname = self::$ceneoImgClass;
            $productList = $this->finder->query("//*[contains(concat(' ', normalize-space(@class), ' '), ' $classname')]");

            if($productList->length == 0)
            {
                $classname = self::$ceneoImgClass2;
                $productList = $this->finder->query("//*[contains(concat(' ', normalize-space(@class), ' '), ' $classname')]");
            }
            foreach ($productList as $key => $value)
            {
                $imageData = base64_encode(file_get_contents('http:'.$value->getElementsByTagName('img')->item(0)->getAttribute("src")));
                if(strlen($imageData) == 0)
                    $imageData = base64_encode(file_get_contents('http:'.$value->getElementsByTagName('img')->item(0)->getAttribute("data-original")));
                $this->productImg[] = $imageData;
            }
            return $this->productImg;

        }
        private function getOtherPages()
        {
            $classname = self::$otherPageClass;
            $productList = $this->finder->query("//*[contains(concat(' ', normalize-space(@class), ' '), ' $classname')]");
            $max = 0;

            foreach ($productList as $value)
            {
                foreach ($value->getElementsByTagName('a') as $link)
                {
                        $lastPage = explode("-", $link->getAttribute("href"));
                        $this->pages[] = $lastPage;
                        if(isset($lastPage[5]))
                            $lastPage = explode(".",$lastPage[5]);
                        else
                            $lastPage = explode(".",$lastPage[4]);
                        if($max <= $lastPage[0])
                            $max = $lastPage[0];
                }
            }
            $link = $this->pages[0][5];
            $tab[] = $this->url;
            $this->pages[0][0] = 'http://www.ceneo.pl'.$this->pages[0][0];
            for($i = 1; $i <=$max; ++$i)
            {
                if(isset($lastPage[5]))
                   $this->pages[0][5] = $i.'.htm';
                else
                    $this->pages[0][4] = $i.'.htm';

                $result = implode("-", $this->pages[0]);
                $tab[] = $result;
            }
            $this->url = $url;
            $this->pageContent = file_get_contents($url);
            $dom = new DOMDocument();
            $dom->loadHTML($this->pageContent);

            return $tab;
        }
        private function getPageContent($_url)
        {
            $this->pageContent = file_get_contents($_url);
            $dom = new DOMDocument();
            $dom->loadHTML($this->pageContent);
            $this->finder = new DomXPath($dom);
        }
        public function getProductFromCategory()
        {
            foreach ($this->getOtherPages() as $page)
            {
                $this->getPageContent($page);
                $this->getProductName();
                $this->getProductPrice();
                $this->getProductImg();
            }
            $tab[] = $this->productName;
            $tab[] = $this->productPrice;
            $tab[] = $this->productImg;
            return $tab;
        }
}
?>