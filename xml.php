<?php

    class Xml
    {
        private $content = null;
        function __construct()
        {
        }
        public function generateXML($tablica)
        {
            $xml = new SimpleXMLElement('<xml/>');
            for ($i = 0; $i<count($tablica[0]); ++$i)
            {
                $track = $xml->addChild('produkt');
                $track->addChild('nazwa', $tablica[0][$i]);
                $track->addChild('cena', $tablica[1][$i]);
                $track->addChild('zdjęcie_base64', $tablica[2][$i]);
            }
            file_put_contents("kategoria.xml", $xml->asXML());
            $file = basename('kategoria.xml');

            if(!$file)
            {
                die('file not found');
            }
            else
            {
                header("Cache-Control: public");
                header("Content-Description: File Transfer");
                header("Content-Disposition: attachment; filename=$file");
                header("Content-Type: application/xml");
                header("Content-Transfer-Encoding: binary");
                readfile($file);
            }
        }
    }
 ?>